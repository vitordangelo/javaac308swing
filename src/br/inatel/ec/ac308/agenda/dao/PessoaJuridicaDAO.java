package br.inatel.ec.ac308.agenda.dao;

import br.inatel.ec.ac308.agenda.dados.Contato;
import br.inatel.ec.ac308.agenda.dados.PessoaFisica;
import br.inatel.ec.ac308.agenda.dados.PessoaJuridica;
import br.inatel.ec.ac308.agenda.dao.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PessoaJuridicaDAO {

    private final Connection connection;

    public PessoaJuridicaDAO() {
        this.connection = new ConnectionFactory().getConnection();
    }

    public void adicionaContato(Contato contato) {
        String sql = "insert into contato "
                + "(nome,endereco,telefone)"
                + " values (?,?,?)";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);

            stmt.setString(1, contato.getNome());
            stmt.setString(2, contato.getEndereco());
            stmt.setString(3, contato.getTelefone());
            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    

}
