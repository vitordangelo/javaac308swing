package br.inatel.ec.ac308.agenda.dao;

import br.inatel.ec.ac308.agenda.dados.PessoaFisica;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PessoaFisicaDAO {

    private final Connection connection;

    public PessoaFisicaDAO() {
        this.connection = new ConnectionFactory().getConnection();
    }

    public void adicionaPessoaFisica(PessoaFisica pessoafisica) {
        String sql = "insert into pessoafisica "
                + "(nome, endereco, telefone, cpf,rg,tiporg)"
                + " values (?,?,?,?,?,?)";
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);

            stmt.setString(1, pessoafisica.getNome());
            stmt.setString(2, pessoafisica.getEndereco());
            stmt.setString(3, pessoafisica.getTelefone());
            stmt.setString(4, pessoafisica.getCpf());
            stmt.setString(5, pessoafisica.getIdentidade());
            stmt.setString(6, pessoafisica.getTipoIdentidade());
            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<PessoaFisica> getListaPessoaFisica() {
        try {
            List<PessoaFisica> pessoasfisica = new ArrayList<PessoaFisica>();
            PreparedStatement stmt = this.connection.
                    prepareStatement("select * from pessoafisica");
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                PessoaFisica pessoafisica = new PessoaFisica();

                pessoafisica.setNome(rs.getString("nome"));
                pessoafisica.setTelefone(rs.getString("telefone"));
                pessoafisica.setEndereco(rs.getString("endereco"));
                pessoafisica.setCpf(rs.getString("cpf"));
                pessoafisica.setIdentidade(rs.getString("rg"));
                pessoafisica.setTipoIdentidade(rs.getString("tiporg"));

                pessoasfisica.add(pessoafisica);
            }
            rs.close();
            stmt.close();
            return pessoasfisica;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void updatePessoaFisica(PessoaFisica pessoafisica) {

        try {
            String sql = "update pessoafisica set endereco=?, telefone=?,"
                    + " cpf=?, rg=?,tiporg=? where nome=?";

            PreparedStatement stmt = connection.prepareStatement(sql);

            stmt.setString(1, pessoafisica.getEndereco());
            stmt.setString(2, pessoafisica.getTelefone());
            stmt.setString(3, pessoafisica.getCpf());
            stmt.setString(4, pessoafisica.getIdentidade());
            stmt.setString(5, pessoafisica.getTipoIdentidade());
            stmt.setString(6, pessoafisica.getNome());

            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void deletaPessoaFisica(PessoaFisica pessoafisica) {

        try {
            String sql = "delete from pessoafisica where nome=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, pessoafisica.getNome());

            stmt.execute();
            stmt.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
