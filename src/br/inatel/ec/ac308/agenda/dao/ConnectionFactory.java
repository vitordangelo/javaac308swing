package br.inatel.ec.ac308.agenda.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

    public Connection getConnection() {
        try {
            return DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/ac308-vitor", "root", "");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
