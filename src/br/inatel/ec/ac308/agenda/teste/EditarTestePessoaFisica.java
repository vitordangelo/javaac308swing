package br.inatel.ec.ac308.agenda.teste;

import br.inatel.ec.ac308.agenda.dados.PessoaFisica;
import br.inatel.ec.ac308.agenda.dao.PessoaFisicaDAO;
import java.sql.SQLException;

public class EditarTestePessoaFisica {

    public static void main(String[] args) throws SQLException {

        PessoaFisica pessoafisica = new PessoaFisica();

        pessoafisica.setEndereco("Rua dos Abacates");
        pessoafisica.setTelefone("Centro");
        pessoafisica.setCpf("345676");
        pessoafisica.setIdentidade("345676");
        pessoafisica.setTipoIdentidade("UIY");
        pessoafisica.setNome("Ivan");

        PessoaFisicaDAO dao = new PessoaFisicaDAO();
        dao.updatePessoaFisica(pessoafisica);
    }
}


