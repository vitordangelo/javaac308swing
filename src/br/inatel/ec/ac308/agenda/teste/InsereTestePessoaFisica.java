package br.inatel.ec.ac308.agenda.teste;

import br.inatel.ec.ac308.agenda.dados.PessoaFisica;
import br.inatel.ec.ac308.agenda.dao.PessoaFisicaDAO;

public class InsereTestePessoaFisica {

    public static void main(String[] args) {
        PessoaFisica pessoafisica = new PessoaFisica();
        pessoafisica.setNome("Vitor");
        pessoafisica.setEndereco("Centro");
        pessoafisica.setTelefone("12345");
        pessoafisica.setCpf("12345");
        pessoafisica.setIdentidade("555");
        pessoafisica.setTipoIdentidade("SSP");

        PessoaFisicaDAO DAO = new PessoaFisicaDAO();
        DAO.adicionaPessoaFisica(pessoafisica);
        System.out.println("Gravado!");

    }
}
