package br.inatel.ec.ac308.agenda.teste;

import br.inatel.ec.ac308.agenda.dados.PessoaFisica;
import br.inatel.ec.ac308.agenda.dao.PessoaFisicaDAO;
import java.sql.SQLException;

public class RemoveTestePessoaFisica {

    public static void main(String[] args) throws SQLException {
        PessoaFisica pessoafisica = new PessoaFisica();

        pessoafisica.setNome("Vitor");

        PessoaFisicaDAO dao = new PessoaFisicaDAO();
        dao.deletaPessoaFisica(pessoafisica);
    }
}
