package br.inatel.ec.ac308.agenda.teste;

import br.inatel.ec.ac308.agenda.dados.Contato;
import br.inatel.ec.ac308.agenda.dados.PessoaFisica;
import br.inatel.ec.ac308.agenda.dao.PessoaFisicaDAO;
import java.sql.SQLException;
import java.util.List;

public class ListarTestePessoaFisica {

    public static void main(String[] args) throws SQLException {
        PessoaFisicaDAO dao = new PessoaFisicaDAO();
        Contato dao1 = new Contato();

        List<PessoaFisica> pessoasfisica = dao.getListaPessoaFisica();

        for (PessoaFisica pessoafisica : pessoasfisica) {
            System.out.println("Nome: " + pessoafisica.getNome());
            System.out.println("Telefone: " + pessoafisica.getTelefone());
            System.out.println("Endereço: " + pessoafisica.getEndereco());
            System.out.println("CPF: " + pessoafisica.getCpf());
            System.out.println("Identidade: " + pessoafisica.getIdentidade());
            System.out.println("Tipo Indentidade: " + pessoafisica.getTipoIdentidade());
            System.out.println("_____________________________________________");
        }
    }
}
