package br.inatel.ec.ac308.agenda.dados;

public class PessoaFisica extends Contato{

    private String cpf;
    private String identidade;
    private String tipoIdentidade;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public String getTipoIdentidade() {
        return tipoIdentidade;
    }

    public void setTipoIdentidade(String tipoIdentidade) {
        this.tipoIdentidade = tipoIdentidade;
    }

}
